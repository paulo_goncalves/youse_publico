package br.com.dbvtech.feitoprapassar.activities;

import android.app.ActivityOptions;
import android.content.Intent;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.test.ActivityUnitTestCase;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import br.com.dbvtech.feitoprapassar.adapters.MainAdapter;
import br.com.dbvtech.feitoprapassar.api.Api;
import br.com.dbvtech.feitoprapassar.beans.Data;
import br.com.dbvtech.feitoprapassar.util.ItemClickSupport;
import cz.msebera.android.httpclient.Header;

import static org.junit.Assert.*;

/**
 * Created by paulo on 27/02/17.
 */
public class MainActivityTest extends ActivityUnitTestCase<MainActivity> {

    public MainActivityTest(){
        super(MainActivity.class);
    }

    @Test
    public void getItens() throws Exception {
        Api.get(Api.ANDROID_NEW, null, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {

                    JSONArray array = response.optJSONObject("data").optJSONArray("children");
                    Gson gson = new Gson();
                    final List<Data> dados = new ArrayList<Data>();
                    for(int i=0; i < array.length(); i++){
                        Data data = gson.fromJson(array.getJSONObject(i).optJSONObject("data").toString(), Data.class);
                        dados.add(data);
                    }

                    if(dados != null){
                        if(dados.size() > 0){
                            assertEquals("Carga da lista", 1, dados.size());
                        }
                    }
                }
                catch (Exception ex){
                    Log.e("MainActivity", ex.getLocalizedMessage() == null ? "" : ex.getLocalizedMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }
        });
    }

}