package br.com.dbvtech.feitoprapassar.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.List;

import br.com.dbvtech.feitoprapassar.R;
import br.com.dbvtech.feitoprapassar.beans.Data;
import br.com.dbvtech.feitoprapassar.beans.Reddit;
import br.com.dbvtech.feitoprapassar.util.ItemClickSupport;

/**
 * Created by paulo on 20/02/17.
 */

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder>{

    private Context context;
    private List<Data> dados;

    public MainAdapter(Context context, List<Data> dados){
        this.context = context;
        this.dados = dados;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        context = parent.getContext();
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Data data = dados.get(position);
        holder.tvTitle.setText(data.getTitle());
        holder.tvQuantidadeComentarios.setText("0 comentários");
        if(data.getThumbnail() != null && !data.getThumbnail().isEmpty() && !data.getThumbnail().equals("self")){
            Picasso.with(context).load(data.getThumbnail()).into(holder.ivImagem);
        }
        else{
            holder.ivImagem.setVisibility(View.GONE);
        }
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemCount() {
        return dados.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        ImageView ivImagem;
        TextView tvQuantidadeComentarios;

        public ViewHolder(View view){
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            ivImagem = (ImageView) view.findViewById(R.id.imagem);
            tvQuantidadeComentarios = (TextView) view.findViewById(R.id.tvQuantidadeComentarios);
        }
    }
}
