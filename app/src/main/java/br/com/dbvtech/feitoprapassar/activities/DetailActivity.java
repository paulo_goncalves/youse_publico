package br.com.dbvtech.feitoprapassar.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.WindowFeature;

import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.dbvtech.feitoprapassar.R;
import br.com.dbvtech.feitoprapassar.beans.Data;

@EActivity(R.layout.activity_detail)
@WindowFeature(Window.FEATURE_NO_TITLE)
public class DetailActivity extends AppCompatActivity {

    private Context context;
    private Data data;

    @ViewById
    TextView tvTitle;

    @ViewById
    ImageView thumbnail;

    @ViewById
    TextView author;

    @ViewById
    TextView postTime;

    @ViewById
    TextView score;

    @ViewById
    WebView webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this;
    }

    @AfterViews
    protected void continua(){
        if(getIntent().hasExtra("dado")){
            data = (Data) getIntent().getSerializableExtra("dado");
            if(data != null){
                if(data.getTitle() != null && !data.getTitle().isEmpty()){
                    tvTitle.setText(data.getTitle());
                }
                else{
                    tvTitle.setText("");
                }

                if(data.getThumbnail() != null && !data.getThumbnail().isEmpty() && !data.getThumbnail().equals("self")){
                    Picasso.with(context).load(data.getThumbnail()).into(thumbnail);
                }
                else{
                    thumbnail.setVisibility(View.INVISIBLE);
                }

                if(data.getAuthor() != null && !data.getAuthor().isEmpty()){
                    author.setText(data.getAuthor());
                }
                else{
                    author.setText("");
                }

                if(data.getCreated_utc() != null && !data.getCreated_utc().isEmpty()){
                    try {
                        Date utc = new Date(Long.parseLong(data.getCreated_utc()));
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                        postTime.setText(sdf.format(utc));
                    }
                    catch (Exception ex){
                        postTime.setText("");
                    }
                }
                else{
                    postTime.setText("");
                }

                if(data.getScore() != null && !data.getScore().isEmpty()){
                    score.setText(data.getScore());
                }
                else{
                    score.setText("");
                }

                if(data.getUrl() != null && !data.getUrl().isEmpty()){
                    WebSettings settings = webview.getSettings();
                    settings.setJavaScriptEnabled(true);
                    webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

                    webview.setWebViewClient(new WebViewClient() {
                        public boolean shouldOverrideUrlLoading(WebView view, String url) {
                            view.loadUrl(url);
                            return true;
                        }

                        public void onPageFinished(WebView view, String url) {
                        }

                        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                        }
                    });

                    webview.loadUrl(data.getUrl());
                }
            }
            else{
                finish();
            }
        }
    }
}
