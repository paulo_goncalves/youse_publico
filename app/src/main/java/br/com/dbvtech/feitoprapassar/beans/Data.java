package br.com.dbvtech.feitoprapassar.beans;

import java.io.Serializable;
import java.util.List;

/**
 * Created by paulo on 20/02/17.
 */

public class Data implements Serializable{
    private String modhash;
    private String after;
    private String before;
    private List<Data> children;
    private boolean contest_mode;
    private String banned_by;
    private String subreddit;
    private String id;
    private String author;
    private String name;
    private String domain;
    private String thumbnail;
    private String permalink;
    private String url;
    private String title;
    private String created_utc;
    private String score;

    public Data() {
    }

    public Data(String modhash, String after, String before, List<Data> children, boolean contest_mode, String banned_by, String subreddit, String id, String author, String name, String domain, String thumbnail, String permalink, String url, String title, String created_utc, String score) {
        this.modhash = modhash;
        this.after = after;
        this.before = before;
        this.children = children;
        this.contest_mode = contest_mode;
        this.banned_by = banned_by;
        this.subreddit = subreddit;
        this.id = id;
        this.author = author;
        this.name = name;
        this.domain = domain;
        this.thumbnail = thumbnail;
        this.permalink = permalink;
        this.url = url;
        this.title = title;
        this.created_utc = created_utc;
        this.score = score;
    }

    public String getModhash() {
        return modhash;
    }

    public void setModhash(String modhash) {
        this.modhash = modhash;
    }

    public String getAfter() {
        return after;
    }

    public void setAfter(String after) {
        this.after = after;
    }

    public String getBefore() {
        return before;
    }

    public void setBefore(String before) {
        this.before = before;
    }

    public List<Data> getChildren() {
        return children;
    }

    public void setChildren(List<Data> children) {
        this.children = children;
    }

    public boolean isContest_mode() {
        return contest_mode;
    }

    public void setContest_mode(boolean contest_mode) {
        this.contest_mode = contest_mode;
    }

    public String getBanned_by() {
        return banned_by;
    }

    public void setBanned_by(String banned_by) {
        this.banned_by = banned_by;
    }

    public String getSubreddit() {
        return subreddit;
    }

    public void setSubreddit(String subreddit) {
        this.subreddit = subreddit;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getPermalink() {
        return permalink;
    }

    public void setPermalink(String permalink) {
        this.permalink = permalink;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreated_utc() {
        return created_utc;
    }

    public void setCreated_utc(String created_utc) {
        this.created_utc = created_utc;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }
}
