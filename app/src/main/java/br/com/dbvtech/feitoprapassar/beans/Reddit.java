package br.com.dbvtech.feitoprapassar.beans;

import java.io.Serializable;
import java.util.List;

/**
 * Created by paulo on 20/02/17.
 */

public class Reddit implements Serializable{
    private String kind;
    private Data data;

    public Reddit() {
    }

    public Reddit(String kind, Data data) {
        this.kind = kind;
        this.data = data;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
