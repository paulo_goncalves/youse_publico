package br.com.dbvtech.feitoprapassar.activities;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.WindowFeature;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.dbvtech.feitoprapassar.R;
import br.com.dbvtech.feitoprapassar.adapters.MainAdapter;
import br.com.dbvtech.feitoprapassar.api.Api;
import br.com.dbvtech.feitoprapassar.beans.Data;
import br.com.dbvtech.feitoprapassar.beans.Reddit;
import br.com.dbvtech.feitoprapassar.util.ItemClickSupport;
import cz.msebera.android.httpclient.Header;

@EActivity(R.layout.activity_main)
@WindowFeature(Window.FEATURE_NO_TITLE)
public class MainActivity extends AppCompatActivity {

    private Context context;
    private Activity activity;
    private ProgressDialog dialog;

    @ViewById
    RecyclerView list;

    @ViewById
    SwipeRefreshLayout swiperefresh;

    private MainAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this;
        this.activity = this;
    }

    @AfterViews
    protected void continua(){
        dialog = ProgressDialog.show(context, "Carregando", "Aguarde ...");

        getItens();

        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getItens();
            }
        });
    }

    protected void getItens(){
        Api.get(Api.ANDROID_NEW, null, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    JSONArray array = response.optJSONObject("data").optJSONArray("children");
                    Gson gson = new Gson();
                    final List<Data> dados = new ArrayList<Data>();
                    for(int i=0; i < array.length(); i++){
                        Data data = gson.fromJson(array.getJSONObject(i).optJSONObject("data").toString(), Data.class);
                        dados.add(data);
                    }

                    if(dados != null){
                        if(dados.size() > 0){
                            adapter = new MainAdapter(context, dados);
                            list.setLayoutManager(new LinearLayoutManager(context));

                            ItemClickSupport.addTo(list).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                                @Override
                                public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                                    Intent intentDetail = new Intent(context, DetailActivity_.class);
                                    intentDetail.putExtra("dado", dados.get(position));
                                    startActivity(intentDetail,
                                            ActivityOptions.makeSceneTransitionAnimation(activity).toBundle());
                                }
                            });

                            RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
                            itemAnimator.setAddDuration(1000);
                            itemAnimator.setRemoveDuration(1000);
                            list.setItemAnimator(itemAnimator);
                            list.setAdapter(adapter);

                        }
                    }
                }
                catch (Exception ex){
                    Log.e("MainActivity", ex.getLocalizedMessage() == null ? "" : ex.getLocalizedMessage());
                }

                dialog.cancel();
                swiperefresh.setRefreshing(false);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                dialog.cancel();
                swiperefresh.setRefreshing(false);
                Toast.makeText(context, "Houve um problema de comunicação com o servidor. Puxe a lista para recarregar.", Toast.LENGTH_LONG).show();
            }
        });
    }
}
